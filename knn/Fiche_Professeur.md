# Fiche Prof KNN

**Thématique** : Algorithme des k plus proches voisins

**Notions liées** : Données en table de première

**Résumé de l’activité** : découverte de l’algorithme des k plus proches voisins puis implémentation

**Objectifs** : à partir d’un exemple simple, expérimenter cet algorithme pour se l’approprier, puis dans un second temps passer à son implémentation. 

**Auteur** : Marbot

**Durée de l’activité** : 2h

**Forme de participation** : individuelle ou en binôme, en autonomie

**Matériel nécessaire** : Ordinateur et connexion Internet

**Préparation** : Aucune

**Autres références** :

**Fiche élève cours** : Aucune

**Fiche élève activité** : [activité de découverte](https://pixees.fr/informatiquelycee/n_site/nsi_prem_knn.html)






